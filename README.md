# Kernel Fragments

This repository is used for storing files to make it easier to group and tweak
kernel fragments when building the kernel.

# How to build with these fragments

## Tuxsuite build

```
tuxsuite build --git-repo https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git --git-ref master --target-arch arm64 --toolchain gcc-13 --kconfig defconfig --kconfig https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/lkft.config --kconfig https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/lkft-crypto.config --kconfig https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/distro-overrides.config --kconfig https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/systemd.config --kconfig https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/virtio.config --kconfig CONFIG_SYN_COOKIES=y --kconfig CONFIG_SCHEDSTATS=y --kconfig CONFIG_DEBUG_KMEMLEAK=y
```

## Tuxmake build

```
tuxmake --runtime podman --target-arch arm64 --toolchain gcc-13 --kconfig defconfig --kconfig-add https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/lkft.config --kconfig-add https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/lkft-crypto.config --kconfig-add https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/distro-overrides.config --kconfig-add https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/systemd.config --kconfig-add https://gitlab.com/Linaro/lkft/kernel-fragments/-/raw/main/virtio.config --kconfig-add CONFIG_SYN_COOKIES=y --kconfig-add CONFIG_SCHEDSTATS=y --kconfig-add CONFIG_DEBUG_KMEMLEAK=y
```
